import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {
  heroes: string[] = ['Spiderman','ironman','Thor','hulk','captain america'];
  deletedHeroe: string = "";

  delete(){
      this.deletedHeroe = this.heroes.shift() || "";
  }
}
